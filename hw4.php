<!-- Homework 3 strip characters -->
<html>
<body>
<?php
  echo "<p>Homework 4 - Repetition<br/>";
  echo "Randy Kizer<br/>";
  echo "09/16/18<br/></p>";
  
  /* Randy Kizer
     Week 4
     HW3 - Repetition - read assosiative array,sort it, display it
     9/12/18
  */
   /* open file */
   $myfile = "hw3.txt";
   $fp = fopen($myfile, 'r');
   if (!$fp)
   {
	   echo "<p> could not open file.</p>";
   }
  

   /* loop reading until the end of file is found */
   do {
	   $key = fread ($fp, 3);
       $str = fgets($fp, 60); 
       if(feof($fp)){
		   break;
	   }
       /* save read data into array */ 
       $fileList[$key]=$str; 
	   	   
   }while (!feof($fp));
   

   /* sort the array by the key */
   ksort ($fileList);
   
   /* display the sorted array -in a table*/
    echo "<table style=\"width:100\%\"><tr><th width =
	\"30\%\">Key</th><th>String Value</th></tr>";
	
	foreach ($fileList as $k => $val){ 
		echo "<tr><td>$k </td><td>$val</td></tr>";
	}
	
?>
</body>
</html>

